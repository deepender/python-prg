import sys
import time 
sdots=50

count = 0
while (count < 9):
    print ('The count is:' , count)
    count = count + 1

#block 1 module
print()
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

count = 0
while count < 5:
    print (count, " is less than 5" )
    count = count + 1
else:
    print (count, " is not less than 5" )

#block 2 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

flag = 1
while (flag): print ('Given flag is really true!' );flag-=1

#block 3 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

for var in list(range(5)):
    print (var)

#block 4 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()


for letter in 'Python' :  # traversal of a string sequence
    print ('Current Letter :' , letter)
    print()
fruits = [ 'banana' , 'apple' ,  'mango' ]
for fruit in fruits:  # traversal of List sequence
    print ('Current fruit :' , fruit)

fruits = [ 'banana' , 'apple' ,  'mango' ]
for index in range(len(fruits)):
    print ('Current fruit :' , fruits[ index])

#block 5 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

'''The following example illustrates the combination of an else statement with a for
statement that searches for even number in given list.'''
#! /usr/bin/python3
numbers=[ 11, 33, 55, 39, 55, 75, 37, 21, 23, 41, 13]
for num in numbers:
    if num%2==0:
        print ('the list contains an even number' )
        break
else:
    print ('the list doesnot contain even number' )

for i in range(1, 11):
    for j in range(1, 11):
        k=i*j
        print (k, end=' ' )
    print()

#block 6 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

for letter in 'Python' :  # First Example
    if letter == 'h' :
        break
    print ('Current Letter :' , letter)
var = 10 # Second Example
while var > 0:
    print ('Current variable value :' , var)
    var = var -1
    if var == 5:
        break

#block 7 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

no=int(input('any number: ' ))
numbers=[ 11, 33, 55, 39, 55, 75, 37, 21, 23, 41, 13]
for num in numbers:
    if num==no:
        print ('number found in list' )
        break
else:
    print ('number not found in list' )

#block 8 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

for letter in 'Python' :  # First Example
    if letter == 'h' :
        continue
    print ('Current Letter :' , letter)
var = 10 # Second Example
while var > 0:
    var = var -1
    if var == 5:
        continue
    print ('Current variable value :' , var)

#block 9 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()

for letter in 'Python' :
    if letter == 'h' :
        pass
    print ('This is pass block' )
    print ('Current Letter :' , letter)

#block 10 module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(6.0/100.0)
    print('#', end='', flush=True)
    sdots-=1
print('|')
print()


'''Iterator is an object, which allows a programmer to traverse through all the elements of
a collection, regardless of its specific implementation. In Python, an iterator object
implements two methods, iter() and next().
String, List or Tuple objects can be used to create an Iterator.'''
list=[ 1, 2, 3, 4]
it = iter(list) # this builds an iterator object
print (next(it)) #prints next available element in iterator
#Iterator object can be traversed using regular for statement

print("Printing all value in itr:-")

#for x in it:
    #print (x, end=" ")
#or using next() function
while True:
    try:
        print (next(it))
    except StopIteration:
        sys. exit() #you have to import sys module for this
'''A generator is a function that produces or yields a sequence of values using yield method.
When a generator function is called, it returns a generator object without even beginning
execution of the function. When the next() method is called for the first time, the function
starts executing, until it reaches the yield statement, which returns the yielded value. The
yield keeps track i.e. remembers the last execution and the second next() call continues
from previous value.
The following example defines a generator, which generates an iterator for all the Fibonacci
numbers.'''