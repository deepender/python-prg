#Program in Python for value interchange
import time

a = input("Enter value of a =")
b = input("Enter value of b =")

temp = a
a = b
b = temp

print("\nValue after interchange:-\n")

print(" a = " + str(a) +"\n b = " + str(b));

print("\n Program will close in 3 seconds...")

time.sleep(3)


'''
IMPORTANT NOTE:-

In Python 3+, print is a function, so it must be called with its arguments between parentheses. So for example, what you mean with your example:

print ("Type string: ") + str(123)

would actually be the same as:

var = print("Type string: ")
var + str(123)

Since print returns nothing (in Python, this means None), this is the equivalent of:

None + str(123)

which evidently will give an error.

That being said about what you tried to do, what you want do to is very easy: pass the print function what you mean to print, which can be done in various ways:

print ("Type string: " + str(123))

# Using format method to generate a string with the desired contents
print ("Type string: {}".format(123)) 

# Using Python3's implicit concatenation of its arguments, does not work the same in Python2:
print ("Type string:", str(123)) # Notice this will insert a space between the parameters


'''

