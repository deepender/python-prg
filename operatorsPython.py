#this is how we print binary of decimal number
a = 20
b = 10
c = 15
d = 5
print ("a:%d b:%d c:%d d:%d" % (a, b, c, d )) #anaother way of printing
a = 60 # 60 = 0011 1100
b = 13 # 13 = 0000 1101
print (' a = ' ,a ,' : ' ,bin(a) ,' b = ' , b ,' : ', bin(b))
c = 0
c = a & b;  # 12 = 0000 1100
print ("result of AND is ",c ,':' , bin(c))
c = a | b;  # 61 = 0011 1101
print ("result of OR is ", c ,':' , bin(c))
c = a ^ b;  # 49 = 0011 0001
print ("result of EXOR is ", c, ':' , bin(c))
c = ~a;  # -61 = 1100 0011
print ("result of COMPLEMENT is ", c ,':' , bin(c))
c = a << 2;  # 240 = 1111 0000
print ("result of LEFT SHIFT is ", c, ':' , bin(c))
c = a >> 2;  # 15 = 0000 1111
print ("result of RIGHT SHIFT is " , c ,':' , bin(c))
a = 20
b = 20
print ("Line 1" , "a=" , a, ":" , id(a), "b=" , b,":" , id(b))
if ( a is b ):
    print ( "Line 2 - a and b have same identity" )
else:
    print ( "Line 2 - a and b do not have same identity" )
if ( id(a) == id(b) ):
    print ( "Line 3 - a and b have same identity" )
else:
    print ( "Line 3 - a and b do not have same identity" )
    
b = 30
print ('Line 4' , 'a=' , str(a), ':' , id(a), 'b=' , str(b), ':' , id(b))
if ( a is not b ):
    print ( "Line 5 - a and b do not have same identity" )
else:
    print ( "Line 5 - a and b have same identity" )
    
print("\nlast ", end="")
print("line, Enter any key to exit..!")
input()

#comment block
'''
You cannot concatenate a string with an int. 
You would need to convert your int to a string using the str function, or use formatting to format your output.

Change: -

print("Ok. Your balance is now at " + balanceAfterStrength + " skill points.")

to: -

print("Ok. Your balance is now at {} skill points.".format(balanceAfterStrength))

or: -

print("Ok. Your balance is now at " + str(balanceAfterStrength) + " skill points.")

or as per the comment, use , to pass different strings to your print function, rather than concatenating using +: -

print("Ok. Your balance is now at ", balanceAfterStrength, " skill points.")

'''