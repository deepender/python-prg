import time
import sys
import random

print("Displaying random generation...")

#time buffer block module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(4.0/100.0)
    print('*', end='', flush=True)
    sdots-=1
print('|')
print()

print ("returns a random number from range(100) : ",random.choice(range(100)))  
print ("returns random element from list [1, 2, 3, 5, 9]) : ", random.choice([1,2, 3, 5, 9]))
print  ("returns  random  character  from  string  'Hello  World'  :  ",  random.choice('Hello World'))

#time buffer block module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(4.0/100.0)
    print('*', end='', flush=True)
    sdots-=1
print('|')
print()

# randomly select an odd number between 1-100
print ("randrange(1,100, 2) : ", random.randrange(1, 100, 2))  
# randomly select a number between 0-99
print ("randrange(100) : ", random.randrange(100))

#The random() method returns a random floating point number in the range [0.0, 1.0]. 
print("\nNow using random function");
# First random number 
print ("random() : ", random.random())
# Second random number 
print ("random() : ", random.random())

#The  seed() method  initializes  the  basic  random  number  generator.  
#Call  this  function before calling any other random module function. 
#time buffer block module
sdots=20
print("\nBuffering",end=' ', flush=True)
print('==>|', end=' ', flush=True)
while(sdots!=0):
    time.sleep(4.0/100.0)
    print('*', end='', flush=True)
    sdots-=1
print('|')
print()

print("\nRandom Seed")

random.seed()
print ("random number with default seed", random.random())  
random.seed(10)
print ("random number with int seed", random.random())  
random.seed("hello",2)
print ("random number with string seed", random.random())
